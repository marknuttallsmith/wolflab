import os

# Grabs the folder where the script runs.
basedir = os.path.abspath(os.path.dirname(__file__))

# Enable debug mode.
DEBUG = True

# Secret key for session management. You can generate random strings here:
# https://randomkeygen.com/
SECRET_KEY = 'my precious'

# Connect to the database
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'database.db')

GITLAB_CLIENT_ID = '3cb1a59af426c2d1029debbe0a84fd34bdc77a314ea4380fae04b87fa6b7b4ad' #os.getenv('GOOGLE_CLIENT_ID')
GITLAB_CLIENT_SECRET = '994f3a3de89b668661792ce53f9cc31673cef24af57caa7f57829e842ce7f28b' #os.getenv('GOOGLE_CLIENT_SECRET')