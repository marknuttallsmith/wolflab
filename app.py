# ----------------------------------------------------------------------------#
# Imports
# ----------------------------------------------------------------------------#
import logging
from functools import wraps
from logging import Formatter, FileHandler

import gitlab
from authlib.integrations.flask_client import OAuth
from flask import Flask, url_for, session, request
from flask import render_template, redirect

# ----------------------------------------------------------------------------#
# App Config.
# ----------------------------------------------------------------------------#
from forms import NewAppForm

app = Flask(__name__)
app.secret_key = '!secret'
app.config.from_object('config')

CONF_URL = 'https://gitlab.com/.well-known/openid-configuration'
oauth = OAuth(app)
oauth.register(
    name='gitlab',
    server_metadata_url=CONF_URL,
    client_kwargs={
        'scope': 'openid email profile api'
    }
)


# Login required decorator.

def login_required(test):
    @wraps(test)
    def wrap(*args, **kwargs):
        if 'user' in session:
            return test(*args, **kwargs)
        else:
            # flash('You need to login first.')
            return redirect(url_for('login'))

    return wrap


# ----------------------------------------------------------------------------#
# Controllers.
# ----------------------------------------------------------------------------#

@app.route('/')
@login_required
def homepage():
    user = session.get('user')
    token = session.get('token')
    # gl = gitlab.Gitlab('https://gitlab.com', oauth_token=token['access_token'])
    # projects = gl.projects.list(owned=True)
    return render_template('pages/home.html', user=user)


@app.route('/workspace/<workspace>')
@login_required
def workspace(workspace):
    user = session.get('user')
    token = session.get('token')
    gl = gitlab.Gitlab('https://gitlab.com', oauth_token=token['access_token'])
    projects = gl.projects.list(owned=True)

    def project_filter(project):
        return project.namespace['name'] == workspace and not project.archived

    projects = [p for p in projects if project_filter(p)]
    return render_template('pages/workspace.html', user=user, workspace=workspace, projects=projects)


@app.route('/login')
def login():
    redirect_uri = url_for('auth', _external=True)
    return oauth.gitlab.authorize_redirect(redirect_uri)


@app.route('/auth')
def auth():
    token = oauth.gitlab.authorize_access_token()
    # print(token)
    user = oauth.gitlab.parse_id_token(token)
    session['user'] = user
    # DON'T DO IT IN PRODUCTION, SAVE INTO DB IN PRODUCTION
    session['token'] = token
    return redirect('/')


@app.route('/logout')
def logout():
    session.pop('token', None)
    session.pop('user', None)
    return redirect('/')


@app.route('/workspace/<workspace>/newapp', methods=('GET', 'POST'))
def newapp(workspace):
    form = NewAppForm(request.form)

    if form.validate_on_submit():
        name = form.name.data

        user = session.get('user')
        token = session.get('token')
        gl = gitlab.Gitlab('https://gitlab.com', oauth_token=token['access_token'])

        group_id = gl.groups.list(search=workspace)[0].id
        group_with_project_templates_id = gl.groups.list(search='project-templates')[0].id
        project = gl.projects.create({'name': name,
                                      'namespace_id': group_id,
                                      'use_custom_template': True,
                                      'group_with_project_templates_id': group_with_project_templates_id,
                                      'template_project_id': 29497576,
                                      # 'template_name': 'nltrd/templates/app-template'
                                      })

        # redirect the browser to another route and template
        return redirect(url_for('workspace', workspace=workspace))

    return render_template('forms/newapp.html', form=form)


# Error handlers.


@app.errorhandler(500)
def internal_error(error):
    # db_session.rollback()
    return render_template('errors/500.html'), 500


@app.errorhandler(404)
def not_found_error(error):
    return render_template('errors/404.html'), 404


if not app.debug:
    file_handler = FileHandler('error.log')
    file_handler.setFormatter(
        Formatter('%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]')
    )
    app.logger.setLevel(logging.INFO)
    file_handler.setLevel(logging.INFO)
    app.logger.addHandler(file_handler)
    app.logger.info('errors')

# ----------------------------------------------------------------------------#
# Launch.
# ----------------------------------------------------------------------------#

# Default port:
if __name__ == '__main__':
    app.run(debug=True)

# Or specify port manually:
'''
if __name__ == '__main__':
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port)
'''
